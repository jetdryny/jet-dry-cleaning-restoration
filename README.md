Jet Dry Cleaning & Restoration has been Rochester, NY’s premier cleaning and restoration experts for over 20 years. Using state-of-the-art cleaning methods, we assist with air duct cleaning, mold remediation, water damage, fire & smoke damage, carpet cleaning, and more.

Address: 1163 Spencerport Rd, Rochester, NY 14606, USA

Phone: 585-820-6324

Website: https://www.jetdryny.com
